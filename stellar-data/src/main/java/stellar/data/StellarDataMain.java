package stellar.data;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stellar.data.memory.InMemorySystemDAO;

@Configuration
@ComponentScan("stellar")
public class StellarDataMain {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarDataMain.class);
        //new ClassPathXmlApplicationContext("classpath:context.xml");
        SystemDAO dao = context.getBean(SystemDAO.class);
        SystemDAO dao2 = context.getBean(SystemDAO.class);
        //new InMemorySystemDAO();
        dao.getAllPlanetarySystems().forEach(System.out::println);
    }
}
