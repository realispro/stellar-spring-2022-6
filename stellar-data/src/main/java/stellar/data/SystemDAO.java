package stellar.data;

import stellar.model.PlanetarySystem;

import java.util.List;

public interface SystemDAO {

    List<PlanetarySystem> getAllPlanetarySystems();

    List<PlanetarySystem> getPlanetarySystemsByName(String like);

    PlanetarySystem getPlanetarySystem(int id);

    PlanetarySystem addPlanetarySystem(PlanetarySystem system);

}
