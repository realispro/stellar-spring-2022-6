package stellar.data.data;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.data.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
public class DataPlanetDAOAdapter implements PlanetDAO {

    private DataPlanetDAO dataPlanetDAO;

    public DataPlanetDAOAdapter(DataPlanetDAO dataPlanetDAO) {
        this.dataPlanetDAO = dataPlanetDAO;
    }

    @Override
    public List<Planet> getAllPlanets() {
        return dataPlanetDAO.findAll();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return dataPlanetDAO.findPlanetsBySystemId(system.getId());
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return dataPlanetDAO.findById(id).get();
    }

    @Override
    public Planet addPlanet(Planet p) {
        return dataPlanetDAO.saveAndFlush(p);
    }
}
