package stellar.data.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;

import java.util.List;

public interface DataPlanetDAO extends JpaRepository<Planet, Integer> {

    List<Planet> findPlanetsBySystemId(int systemId);
}
