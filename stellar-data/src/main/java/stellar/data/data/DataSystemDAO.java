package stellar.data.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface DataSystemDAO extends JpaRepository<PlanetarySystem, Integer> {

    List<PlanetarySystem> findByNameLike(String name);

}
