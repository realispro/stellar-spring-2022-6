package stellar.data.data;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.data.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
public class DataSystemDAOAdapter implements SystemDAO {

    private DataSystemDAO dataSystemDAO;

    public DataSystemDAOAdapter(DataSystemDAO dataSystemDAO) {
        this.dataSystemDAO = dataSystemDAO;
    }

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return dataSystemDAO.findAll();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return dataSystemDAO.findByNameLike("%" + like + "%");
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return dataSystemDAO.findById(id).get();
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        return dataSystemDAO.saveAndFlush(system);
    }
}
