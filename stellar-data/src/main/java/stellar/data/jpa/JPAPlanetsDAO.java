package stellar.data.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.data.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
public class JPAPlanetsDAO implements PlanetDAO {
    @Override
    public List<Planet> getAllPlanets() {
        return null;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return null;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return null;
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }
}
