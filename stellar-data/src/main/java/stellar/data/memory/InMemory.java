package stellar.data.memory;

import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InMemory {

    static List<PlanetarySystem> systems = new ArrayList<>();

    static {
        PlanetarySystem s = new PlanetarySystem(
                1,
                "Solar System",
                "Sun",
                java.sql.Date.valueOf(LocalDate.of(857, 3, 4)),
                3.234f,
                "https://en.wikipedia.org/wiki/Solar_System");
        s.setPlanets(Arrays.asList(
                new Planet(1, "Mercury", 2439, 3, 0, s),
                new Planet(2, "Venus", 6051, 5, 0, s),
                new Planet(3, "Earth", 6371, 60, 1, s),
                new Planet(4, "Mars", 3389, 6, 2, s),
                new Planet(5, "Jupiter", 69911, 10000, 79, s),
                new Planet(6, "Neptun", 24622, 1000, 14, s),
                new Planet(7, "Saturn", 58232, 5000, 82, s),
                new Planet(8, "Uranus", 25362, 800, 27, s)
        ));
        systems.add(s);
        s = new PlanetarySystem(
                2,
                "Kepler-90 System",
                "Kepler-90",
                java.sql.Date.valueOf(LocalDate.of(1987, 11, 2)),
                12.275123f,
                "https://pl.wikipedia.org/wiki/Kepler-90");
        s.setPlanets(Arrays.asList(
                new Planet(21, "b", s),
                new Planet(22, "c", s),
                new Planet(23, "i", s),
                new Planet(24, "d", s),
                new Planet(25, "e", s),
                new Planet(26, "f", s),
                new Planet(27, "g", s)
        ));
        systems.add(s);
        s = new PlanetarySystem(
                3,
                "Trappist-1 System",
                "Trappist-1",
                java.sql.Date.valueOf(LocalDate.of(2013, 7, 27)),
                7.62946f,
                "https://pl.wikipedia.org/wiki/TRAPPIST-1");
        s.setPlanets(Arrays.asList(
                new Planet(31, "b", s),
                new Planet(32, "c", s),
                new Planet(33, "d", s),
                new Planet(34, "e", s),
                new Planet(35, "f", s),
                new Planet(36, "g", s),
                new Planet(37, "h", s)
        ));
        systems.add(s);
    }


}
