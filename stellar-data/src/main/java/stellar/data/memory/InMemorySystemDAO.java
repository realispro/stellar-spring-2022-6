package stellar.data.memory;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import stellar.data.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Repository
@Lazy
@Scope("prototype")
public class InMemorySystemDAO implements SystemDAO {

    private Logger log = Logger.getLogger(InMemorySystemDAO.class.getName());

    public InMemorySystemDAO() {
        log.info("constructing object of type InMemorySystemDAO");
    }

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return InMemory.systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return InMemory.systems.stream().filter(s -> s.getName().toLowerCase().contains(like.toLowerCase())).collect(Collectors.toList());
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return InMemory.systems.stream().filter(s -> s.getId() == id).findFirst().orElse(null);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        int idMax = InMemory.systems.stream().sorted((s1, s2) -> s2.getId() - s1.getId()).findFirst().get().getId();

        system.setId(++idMax);
        InMemory.systems.add(system);
        return system;
    }
}