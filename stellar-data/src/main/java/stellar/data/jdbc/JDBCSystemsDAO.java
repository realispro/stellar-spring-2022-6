package stellar.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.data.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.logging.Logger;

@Repository("jdbcSystemsDAO")
@Qualifier("db")
public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());


    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where id=?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems = jdbcTemplate.query(SELECT_ALL_SYSTEMS, new SystemMapper());

        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems =
                jdbcTemplate.query(SELECT_SYSTEMS_BY_NAME, new Object[]{"%" + like + "%"}, new SystemMapper());

        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        PlanetarySystem ps =
                jdbcTemplate.queryForObject(SELECT_SYSTEM_BY_ID, new Object[]{id}, new SystemMapper());

        return ps;
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        jdbcTemplate.update(
                "INSERT INTO PLANETARYSYSTEM(NAME, STAR, DISTANCE, DISCOVERY) VALUES (?,?,?,?)",
                system.getName(), system.getStar(), system.getDistance(), system.getDiscovery());

        return system;
    }

}
