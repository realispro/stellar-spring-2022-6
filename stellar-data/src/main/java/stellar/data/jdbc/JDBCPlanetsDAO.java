package stellar.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.data.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.logging.Logger;

@Repository("jdbcPlanetsDAO")
@Qualifier("db")
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());

    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=? and name like ?";

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets =
                jdbcTemplate.query(SELECT_ALL_PLANETS, new PlanetMapper());
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> systems =
                jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new Object[]{system.getId()}, new PlanetMapper());

        return systems;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        /*List<Planet> systems = new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PLANETS_BY_SYSTEM_AND_NAME)) {
            prpstm.setInt(1, system.getId());
            prpstm.setString(2, "%" + like + "%");
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapPlanet(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }*/
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return null;
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }
}
