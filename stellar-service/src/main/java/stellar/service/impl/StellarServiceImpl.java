package stellar.service.impl;


import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.data.PlanetDAO;
import stellar.data.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.logging.Logger;

@Component
public class StellarServiceImpl implements StellarService {

    Logger log = Logger.getLogger(StellarServiceImpl.class.getName());

    //@Autowired
    private SystemDAO systemDAO;
    //@Autowired
    private PlanetDAO planetDAO;

    private ApplicationContext context;

    private PlatformTransactionManager tm;

    public StellarServiceImpl(SystemDAO systemDAO, PlanetDAO planetDAO, ApplicationContext context) {
        this.systemDAO = systemDAO;
        this.planetDAO = planetDAO;
        this.context = context;
    }

    /*@PostConstruct
    public void init() {
        for (String beanName : context.getBeanDefinitionNames()) {
            log.info("bean name: " + beanName + " type: " + context.getBean(beanName).getClass().getSimpleName());
        }
    }*/

    @Override
    public List<PlanetarySystem> getSystems() {
        log.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        log.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        log.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems = systemDAO.getPlanetarySystemsByName(like);
        log.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        log.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        //logg.info("found: " + system);
        return system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        log.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system, like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {

        //TransactionStatus ts = tm.getTransaction(new DefaultTransactionDefinition());
        PlanetarySystem planetarySystem = systemDAO.addPlanetarySystem(s);
        //tm.commit(ts);
        return planetarySystem;
    }


   /* public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }*/
}
