package stellar.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import stellar.data.PlanetDAO;
import stellar.data.SystemDAO;
import stellar.data.memory.InMemoryPlanetDAO;
import stellar.data.memory.InMemorySystemDAO;
import stellar.model.PlanetarySystem;
import stellar.service.impl.StellarServiceImpl;

@Configuration
@ComponentScan("stellar")
public class StellarServiceMain {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarServiceMain.class);
        StellarService service = context.getBean(StellarService.class);
                /*new StellarServiceImpl();
        ((StellarServiceImpl) service).setSystemDAO(new InMemorySystemDAO());
        ((StellarServiceImpl) service).setPlanetDAO(new InMemoryPlanetDAO());*/

        PlanetarySystem system = service.getSystemById(1);
        service.getPlanets(system).forEach(System.out::println);
    }

    @Bean("name")
    public String name() {
        return "Lorem ipsum";
    }

 /*   @Bean
    public StellarService service(PlanetDAO planetDAO, SystemDAO systemDAO) {
        return new StellarServiceImpl("foo bar", systemDAO, planetDAO);
    }*/
}
