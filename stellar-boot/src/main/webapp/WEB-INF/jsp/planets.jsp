<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>
<span>Planets of system ${system.name}</span>
<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${planets}" var="p">
        <tr>
            <td>${p.id}</td>
            <td><a href="./planet?planetId=${p.id}">${p.name}</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>


<jsp:include page="footer.jsp"/>
