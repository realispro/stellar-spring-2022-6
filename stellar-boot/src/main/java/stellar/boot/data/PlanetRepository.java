package stellar.boot.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.boot.model.Planet;

import java.util.List;

public interface PlanetRepository extends JpaRepository<Planet, Integer> {


    List<Planet> findPlanetsBySystemId(int systemId);
}
