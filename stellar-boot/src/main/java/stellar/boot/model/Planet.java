package stellar.boot.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
//@NoArgsConstructor
//@ToString
//@EqualsAndHashCode
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int size;
    private int weight;
    private int moons;

    @ManyToOne
    @JoinColumn(name = "system_id")
    private PlanetarySystem system; // system_id

}
