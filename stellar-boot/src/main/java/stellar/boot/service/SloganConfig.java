package stellar.boot.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SloganConfig {

    @Bean
    @ConditionalOnMissingBean(SloganService.class)
    //@ConditionalOnProperty(value = "stellar.slogan.mock", havingValue = "true")
    public SloganService sloganService() {
        return () -> "mocking slogan service";
    }

}
