package stellar.boot.service;

public interface SloganService {

    String getSlogan();
}
