package stellar.boot.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class SloganServiceBean implements SloganService {

    private final static List<String> slogans = Arrays.asList(
            "Sky is the limit",
            "Per aspera ad astra",
            "Don't look up",
            "Cosmos is within"
    );

    @Override
    public String getSlogan() {
        return slogans.get(new Random(new Date().getTime()).nextInt(slogans.size()));
    }
}
