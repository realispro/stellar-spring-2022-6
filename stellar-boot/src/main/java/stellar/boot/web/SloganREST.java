package stellar.boot.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.boot.service.SloganService;

import java.util.logging.Logger;

@RestController
@RequiredArgsConstructor
@Slf4j
public class SloganREST {

    private final SloganService service;

    @GetMapping("/slogan")
    String getSlogan() {
        String slogan = service.getSlogan();
        log.info("providing slogan: " + slogan);
        return slogan;
    }
}
