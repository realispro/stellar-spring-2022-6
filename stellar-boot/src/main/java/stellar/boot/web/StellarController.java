package stellar.boot.web;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import stellar.boot.data.PlanetRepository;
import stellar.boot.data.SystemRepository;
import stellar.boot.model.PlanetarySystem;

import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/ui")
public class StellarController {

    private final Logger log = Logger.getLogger(StellarController.class.getName());

    private final SystemRepository systemRepository;
    private final PlanetRepository planetRepository;


    public StellarController(SystemRepository systemRepository, PlanetRepository planetRepository) {
        this.systemRepository = systemRepository;
        this.planetRepository = planetRepository;
    }

/*
    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.setValidator(new SystemValidator());
    }
*/


    @GetMapping("/systems")
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "Accept", required = false) List<String> acceptList,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId,
            @RequestHeader/* Map<String, String>*/ HttpHeaders headers
    ) {

        log.info("Accept header received: " + acceptList);
        log.info("Session id sent by a client: " + sessionId);
        //headers.keySet().forEach(key->log.info("header item, key: " + key + ", value: " + headers.get(key)));

        List<PlanetarySystem> systems = phrase == null
                ? systemRepository.findAll()
                : systemRepository.findByNameLike("%" + phrase + "%");
        log.info("found " + systems.size() + " planetary systems");
        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("addSystem")
    public String prepareAddingSystem(Model model) {

        model.addAttribute("systemForm", new PlanetarySystem());

        return "addSystem";
    }

    @PostMapping("addSystem")
    public String addSystem(/*@Validated */@ModelAttribute("systemForm") PlanetarySystem system, BindingResult br) {

        log.info("about to add system " + system.getName());

        if (br.hasErrors()) {
            return "addSystem";
        }

        systemRepository.save(system);

        return "redirect:/systems";
    }


    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId) {

        log.info("about to query planets of a system " + systemId);

        model.addAttribute("planets", planetRepository.findPlanetsBySystemId(systemId));
        model.addAttribute("system", systemRepository.findById(systemId).get());
        return "planets";
    }

    @GetMapping("/planet")
    public String getPlanetById(Model model, @RequestParam("planetId") int planetId) {

        log.info("about to query planet details " + planetId);
        model.addAttribute("planet", planetRepository.findById(planetId).get());

        return "planet";
    }
}
