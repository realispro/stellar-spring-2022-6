<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>
<span>Planet ${planet.name} details</span>
<table>
    <tbody>
    <tr>
        <td>Size:</td>
        <td>${planet.size}</td>
    </tr>
    <tr>
        <td>Weight:</td>
        <td>${planet.weight}</td>
    </tr>
    <tr>
        <td>Moons:</td>
        <td>${planet.moons}</td>
    </tr>
    </tbody>
</table>


<jsp:include page="footer.jsp"/>
