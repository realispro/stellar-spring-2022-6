package stellar.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice(annotations = Adviced.class)
public class StellarAdvice {

    private final static List<String> slogans = Arrays.asList(
            "Sky is the limit",
            "Per aspera ad astra",
            "Don't look up",
            "Cosmos is within"
    );

    private final Logger log = Logger.getLogger(StellarAdvice.class.getName());


    @ModelAttribute
    public void addSlogan(Model model) {
        log.info("adding slogan");
        model.addAttribute("slogan",
                slogans.get(new Random(new Date().getTime()).nextInt(slogans.size())));
    }

    @ExceptionHandler(NoSuchElementException.class)
    public String handleException(NoSuchElementException e, Model model) {

        log.log(Level.SEVERE, "unhandled exception", e);
        model.addAttribute("error_message", e.getMessage());

        return "error";
    }


}
