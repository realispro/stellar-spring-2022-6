package stellar.web.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import stellar.model.PlanetarySystem;

import java.net.MalformedURLException;
import java.net.URL;

public class SystemValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PlanetarySystem.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        PlanetarySystem system = (PlanetarySystem) target;

        if (system.getName() == null || system.getName().trim().isEmpty()) {
            errors.rejectValue("name", "error.name.empty");
        }

        if (system.getStar() == null || system.getStar().trim().isEmpty()) {
            errors.rejectValue("star", "error.star.empty");
        }

        if (system.getDistance() <= 0) {
            errors.rejectValue("distance", "error.distance.negative");
        }

        try {
            system.setDetails(new URL(system.getDetailsString()));
        } catch (MalformedURLException e) {
            errors.rejectValue("details", "error.details.wrong.format");
        }

    }
}
