package stellar.web.rest;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.validation.SystemValidator;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class StellarREST {

    private final Logger log = Logger.getLogger(StellarREST.class.getName());

    private final StellarService service;

    private MessageSource messageSource;

    public StellarREST(StellarService service, MessageSource messageSource) {
        this.service = service;
        this.messageSource = messageSource;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(new SystemValidator());
    }

    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems() {
        log.info("about to fetch systems list");
        return service.getSystems();
    }

    @GetMapping("/systems/{systemId}")
    public PlanetarySystem getSystem(@PathVariable("systemId") int systemId) {
        log.info("about to fetch system " + systemId);
        return service.getSystemById(systemId);
    }

    @GetMapping("/systems/{systemId}/planets")
    public List<Planet> getPlanetsFromSystem(@PathVariable("systemId") int systemId) {
        log.info("list planets from system " + systemId);
        return service.getPlanets(service.getSystemById(systemId));
        //service.getSystemById(systemId).getPlanets();
    }

    @PostMapping("/systems")
    public ResponseEntity addSystem(@Validated @RequestBody PlanetarySystem system, BindingResult br) throws IOException {
        log.info("about to add system " + system);
        if (br.hasErrors()) {
            StringBuilder builder = new StringBuilder("validation fails on: ");
            for (ObjectError e : br.getAllErrors()) {
                builder.append(messageSource.getMessage(e.getCode(), new Object[0], Locale.getDefault()) + "\n");
            }
            return new ResponseEntity(builder.toString(), HttpStatus.BAD_REQUEST);
        }


        system = service.addPlanetarySystem(system);
        return new ResponseEntity(system, HttpStatus.CREATED);
    }


}
