package stellar.web;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class TimeBasedAccessInterceptor implements HandlerInterceptor {

    private int opening;
    private int closing;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        int hour = LocalTime.now().getHour();
        boolean open = hour >= opening && hour < closing;
        if (!open) {
            response.sendRedirect("https://st.depositphotos.com/1561359/4354/v/950/depositphotos_43545063-stock-illustration-stamp-closed-with-red-text.jpg");
        }
        return open;
    }

    public void setOpening(int opening) {
        this.opening = opening;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }
}
