package stellar.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    public String sayHello(Model model) {
        model.addAttribute("hello_text", "First day of summer :)");
        model.addAttribute("slogan", "Don't look up!");
        return "hello";
    }
}
