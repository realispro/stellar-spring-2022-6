package stellar.web;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.validation.SystemValidator;

import java.util.List;
import java.util.logging.Logger;

@Controller
@Adviced
public class StellarController {

    private final Logger log = Logger.getLogger(StellarController.class.getName());

    private final StellarService service;

    public StellarController(StellarService service) {
        this.service = service;
    }

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.setValidator(new SystemValidator());
    }


    @GetMapping("/systems")
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "Accept", required = false) List<String> acceptList,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId,
            @RequestHeader/* Map<String, String>*/ HttpHeaders headers
    ) {

        log.info("Accept header received: " + acceptList);
        log.info("Session id sent by a client: " + sessionId);
        //headers.keySet().forEach(key->log.info("header item, key: " + key + ", value: " + headers.get(key)));

        List<PlanetarySystem> systems = phrase == null ? service.getSystems() : service.getSystemsByName(phrase);
        log.info("found " + systems.size() + " planetary systems");
        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("addSystem")
    public String prepareAddingSystem(Model model) {

        model.addAttribute("systemForm", new PlanetarySystem());

        return "addSystem";
    }

    @PostMapping("addSystem")
    public String addSystem(@Validated @ModelAttribute("systemForm") PlanetarySystem system, BindingResult br) {

        log.info("about to add system " + system.getName());

        if (br.hasErrors()) {
            return "addSystem";
        }

        service.addPlanetarySystem(system);

        return "redirect:/systems";
    }


    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId) {

        log.info("about to query planets of a system " + systemId);

        PlanetarySystem system = service.getSystemById(systemId);

        model.addAttribute("planets", service.getPlanets(system));
        model.addAttribute("system", system);
        return "planets";
    }

    @GetMapping("/planet")
    public String getPlanetById(Model model, @RequestParam("planetId") int planetId) {

        log.info("about to query planet details " + planetId);
        model.addAttribute("planet", service.getPlanetById(planetId));

        return "planet";
    }
}
